import { defineStore } from 'pinia'
import type User from '~/Interfaces/User';
import { getData, setData } from 'nuxt-storage/local-storage';

export const useAuthenticateStore = defineStore('authenticate', () => {
    const user = ref<User | null>();


    onMounted(() => {
        user.value = JSON.parse(getData("user")) as User || null
    });

    const isLogged = computed<boolean>(() => {
        if (user.value) {
            return true;
        }
        return false
    });

    function setLoggedUser(u: User) {
        user.value = u
        setData("user", JSON.stringify(u));
    }

    function logout() {
        user.value = null
        setData("user", "");
    }

    return { isLogged, user, setLoggedUser, logout }
})