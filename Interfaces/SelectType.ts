export default interface SelectType {
    label: String,
    value: String,
};