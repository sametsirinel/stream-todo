export default interface Label {
    label: String,
    created_at: String,
    id?: Number,
};