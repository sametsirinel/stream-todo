export default interface Label {
    title: String,
    description: String,
    label_id: Number,
    created_at: String,
    id?: Number,
};