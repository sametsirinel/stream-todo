import * as moment from "moment";

export default interface Counter {
    title: string,
    description: string,
    started_at: string,
    ended_at: string,
    created_at: string,
    color: string,
    second: number,
    isBg: boolean,
    isTitle: boolean,
    isBorder: boolean,
    id?: string,
};