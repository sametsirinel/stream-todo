// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/tailwindcss',
    '@pinia/nuxt'
  ],
  pinia: {
    storesDirs: ['./stores/**',],
  },
  vite: {
    server: {
      proxy: {
        '/api': {
          target: 'http://json-server:8000',
          changeOrigin: true,
          rewrite: (path) => {
            console.log("path", path)
            return path.replace(/^\/api/, '')
          }
        }
      },
    }
  },
})
